package org.maximchuk.teamcity.api.exception;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Maxim L. Maximcuhk
 *         Date: 07.01.14
 */
public class HttpException extends Exception {
    private int errorCode;
    private String reasonPhrase;
    private String body;

    public HttpException(HttpResponse response) {
        StatusLine statusLine = response.getStatusLine();
        errorCode = statusLine.getStatusCode();
        reasonPhrase = statusLine.getReasonPhrase();
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
            body = reader.readLine();
        } catch (IOException ex) {
            //ignore
        }

    }

    @Override
    public String getMessage() {
        return String.valueOf(errorCode) + " " + reasonPhrase + " " + body;
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getReasonPhrase() {
        return reasonPhrase;
    }

    public String getBody() {
        return body;
    }
}
