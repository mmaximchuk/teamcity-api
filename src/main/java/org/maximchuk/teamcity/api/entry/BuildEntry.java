package org.maximchuk.teamcity.api.entry;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.Root;

import java.io.Serializable;

/**
 * @author Maxim L. Maximcuhk
 *         Date: 07.01.14
 */
@Root(name = "build", strict = false)
public class BuildEntry implements Serializable {

    private static final long serialVersionUID = 4097267721561044491L;

    @Attribute
    private Integer id;

    @Attribute
    private String number;

    @Attribute
    private String status;

    @Attribute
    private String webUrl;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getWebUrl() {
        return webUrl;
    }

    public void setWebUrl(String webUrl) {
        this.webUrl = webUrl;
    }

    @Override
    public String toString() {
        return number;
    }
}
