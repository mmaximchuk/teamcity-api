package org.maximchuk.teamcity.api.provider.impl;

import org.maximchuk.teamcity.api.entry.BuildTypeEntry;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * @author Maxim L. Maximcuhk
 *         Date: 07.01.14
 */
@Root
class BuildTypesParent {

    @ElementList(inline = true, required = false)
    private List<BuildTypeEntry> buildTypes;

    public List<BuildTypeEntry> getBuildTypes() {
        return buildTypes;
    }

    public void setBuildTypes(List<BuildTypeEntry> buildTypes) {
        this.buildTypes = buildTypes;
    }
}
