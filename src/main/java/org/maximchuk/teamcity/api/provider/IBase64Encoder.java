package org.maximchuk.teamcity.api.provider;

/**
 * @author Maxim L. Maximcuhk
 *         Date: 08.01.14
 */
public interface IBase64Encoder {

    public String encode(byte[] data);

}
