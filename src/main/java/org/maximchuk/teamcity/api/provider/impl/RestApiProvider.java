package org.maximchuk.teamcity.api.provider.impl;

import org.apache.http.HttpResponse;
import org.maximchuk.teamcity.api.entry.BuildEntry;
import org.maximchuk.teamcity.api.entry.BuildTypeEntry;
import org.maximchuk.teamcity.api.entry.ProjectEntry;
import org.maximchuk.teamcity.api.exception.HttpException;
import org.maximchuk.teamcity.api.provider.AbstractApiProvider;
import org.maximchuk.teamcity.api.provider.BasicAuthorizationHeader;
import org.maximchuk.teamcity.api.provider.IBase64Encoder;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.List;

/**
 * @author Maxim L. Maximcuhk
 *         Date: 06.01.14
 */
public class RestApiProvider extends AbstractApiProvider {

    public RestApiProvider(String servername, String username, String password, IBase64Encoder encoder) {
        super(servername, new BasicAuthorizationHeader(username, password, encoder));
    }

    public List<ProjectEntry> getProjects() throws Exception {
        ProjectsParent projectsParent = executeGet(ProjectsParent.class, "/httpAuth/app/rest/projects");
        return projectsParent != null? projectsParent.getProjects(): null;
    }

    public List<BuildTypeEntry> getBuildTypes(ProjectEntry project) throws Exception {
        StringBuilder pathBuilder = new StringBuilder("/httpAuth/app/rest/projects");
        pathBuilder.append("/id:").append(project.getId()).append("/buildTypes");

        BuildTypesParent buildTypesParent = executeGet(BuildTypesParent.class, pathBuilder.toString());
        return buildTypesParent != null? buildTypesParent.getBuildTypes(): null;
    }

    public List<BuildEntry> getBuilds(BuildTypeEntry buildType) throws Exception {
        StringBuilder pathBuilder = new StringBuilder("/httpAuth/app/rest/buildTypes");
        pathBuilder.append("/id:").append(buildType.getId()).append("/builds");

        BuildsParent buildsParent = executeGet(BuildsParent.class, pathBuilder.toString());
        return buildsParent != null? buildsParent.getBuilds(): null;
    }

    public void startBuild(BuildTypeEntry buildType) throws Exception {
        StringBuilder pathBuilder = new StringBuilder("/httpAuth/action.html?add2Queue=");
        pathBuilder.append(buildType.getId());

        executeGet(pathBuilder.toString());
    }

    public boolean checkConnection() throws HttpException {
        String responseString = new String();
        try {
            HttpResponse httpResponse = executeGet("/httpAuth/app/rest");
            BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
            responseString = reader.readLine();
        } catch (Exception ex) {
            if (ex instanceof HttpException) {
                throw (HttpException)ex;
            }
        }
        return responseString.contains("This is a root of TeamCity REST API.");
    }

}
