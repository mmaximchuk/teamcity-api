package org.maximchuk.teamcity.api.provider.impl;

import org.maximchuk.teamcity.api.entry.ProjectEntry;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

/**
 * @author Maxim L. Maximcuhk
 *         Date: 07.01.14
 */
@Root
class ProjectsParent {

    @ElementList(inline = true)
    private List<ProjectEntry> projects;

    public List<ProjectEntry> getProjects() {
        return projects;
    }

    public void setProjects(List<ProjectEntry> projects) {
        this.projects = projects;
    }
}
